const express = require("express")
const app = express();
const cors = require("cors");
const models = require("./models")
const { Users, products } = models;
const port = 5000;
const Jwt = require('jsonwebtoken');
const jwtKey = 'ray';
app.use(express.json());
app.use(cors());


app.post("/register", async (req, resp) => {
    try {
        const user = await Users.create(req.body);
        delete user.password
        Jwt.sign({ user }, jwtKey, { expiresIn: "2h" }, (err, token) => {
            if (err) {
                resp.send("Something went wrong")
            }
            resp.send({ user, auth: token })
        })
    }
    catch (err) {
        resp.send(err)
    }
})

app.post("/login", async (req, resp) => {
    try {
        if (req.body.password && req.body.email) {
            const user = await Users.findOne(req.body)
            if (user) {
                Jwt.sign({ user }, jwtKey, { expiresIn: "2h" }, (err, token) => {
                    if (err) {
                        resp.send("Something went wrong")
                    }
                    resp.send({ user, auth: token })
                })
            } else {
                resp.send({ result: "No User found" })
            }
        } else {
            resp.send({ result: "No User found" })
        }
    }
    catch (err) {
        resp.send(err)
    }
});

app.post("/add-product", async (req, resp) => {
    try {
        const product = await products.create(req.body);
        console.log(req.body)
        resp.send(product);
    }
    catch (err) {
        resp.send(err)
    }
});

app.get("/products", async (req, resp) => {
    try {
        const product = await products.findAll();
        if (product.length > 0) {
            resp.send(product)
        } else {
            resp.send({ result: "No Product found" })
        }
    }
    catch (err) {
        resp.send(err)
    }
});


app.delete("/product/:id", async (req, resp) => {
    try {
        const { id } = req.params;
        const result = await products.destroy(
            { where: { id } }
        )
        resp.send(result)
    }
    catch (err) {
        resp.send(err)
    }
})

app.get("/product/:id", async (req, resp) => {
    try {
        const { id } = req.params;
        let result = await products.findOne({ where: { id } })
        if (result) {
            resp.send(result)
        } else {
            resp.send({ "result": "No Record Found." })
        }
    }
    catch (err) {
        resp.send(err)
    }
})

app.put("/product/:id", async (req, resp) => {
    try {
        const result = await products.update(
            {
                name: req.body.name,
                price: req.body.price,
                category: req.body.category,
                company: req.body.company
            },
            { where: { id: req.params.id } },
        )
        resp.send(result)
    }
    catch (err) {
        resp.send(err)
    }
});

// app.put("product/:id",async(req,resp)=>{
//     try{
// const result = await products.findByPk({where:{id:req.params.id}})
// const data = await result.update(
//     {name:req.body.name,
//         price:req.body.price,
//         category:req.body.category,
//         company:req.body.company
//     }
// )
// resp.send(data)
//     }
//     catch (err){
//      resp.send(err)
//     }
// })


// app.get("/search/:key", async (req, resp) => {
//     const result = await products.findAll({
//         "$or": [
//             {
//                 name: { $regex: req.params.key }
//             },
//             {
//                 company: { $regex: req.params.key }
//             },
//             {
//                 category: { $regex: req.params.key }
//             }
//         ]
//     });
//     resp.send(result);
// })




app.listen(port, () => {
    console.log(`running on port ${port}`)
});